let special_sections_keys = ["image"; "stages"; "variables"; "include"]

let field_is_template key = '.' = key.[0]

let field_is_special_section key = List.mem key special_sections_keys

let partition (ci_config : (string * Yaml.value) list) :
    (string * Yaml.value) list
    * (string * Yaml.value) list
    * (string * Yaml.value) list =
  let (special_sections, templates, jobs) =
    List.fold_left
      (fun (special_sections, templates, jobs) ((k, _) as kb) ->
        if field_is_special_section k then
          (kb :: special_sections, templates, jobs)
        else if field_is_template k then
          (special_sections, kb :: templates, jobs)
        else (special_sections, templates, kb :: jobs))
      ([], [], [])
      ci_config
  in
  (List.rev special_sections, List.rev templates, List.rev jobs)

(** Finds and returns all includes, merged *)
let get_other_yaml_paths base_dir (yaml_object : (string * Yaml.value) list) :
    (string * Yaml.value) list =
  let include_paths =
    match List.find_opt (fun (key, _) -> "include" = key) yaml_object with
    | Some (_, include_values) ->
       (match include_values with
        | `A include_list ->
           List.mapi
             (fun idx v ->
               match v with
               | `String path -> path
               | _ -> failwith (Format.asprintf "String expected in include at index %d, value: %s" idx (Yaml.to_string_exn v)))
             include_list
        | `String include_path -> [include_path]
        | _ ->
           failwith "Unexpected type for the `include` key: expected array or string")
    | None -> []
  in
  List.concat @@ List.map
                   (fun path ->
                     let yaml =
                       Yaml_unix.of_file_exn Fpath.(base_dir // v path)
                     in
                     match yaml with
                     | `O yaml_object ->
                        yaml_object
                     | _ ->
                        failwith
                        @@ Format.asprintf
                             "Expected YAML object in file %s"
                             path )
                   include_paths

(** Optionally returns the template this job extends *)
let get_extend_names (job_desc : (string * Yaml.value) list) =
  match List.assoc_opt "extends" job_desc with
  | None ->
      []
  | Some (`String s) ->
      [s]
  | Some (`A ss) ->
      List.map YAML.of_string_exn ss
  | _ ->
      failwith @@ "Expected string value reading `extends` field of: %a"
      ^ ", when reading: "
      ^ Yaml.to_string_exn ~scalar_style:`Plain (`O job_desc)

let _debug = true

(** Replaces the [extends: v] key-value pair, if present, into an obj
   with the key- values in [extend_content] *)
let replace_extend_by_definition ~debug_template_extension job_name job_desc
    extend_content =
  let desc_sans_extends = YAML.object_rem "extends" job_desc in
  let desc_sans_extends =
    if debug_template_extension then
      YAML.object_prepend_field
        "__glcisk_job_definition"
        (`String job_name)
        desc_sans_extends
    else desc_sans_extends
  in
  YAML.merge_objects_append ~recursive:true [extend_content; desc_sans_extends]

let _check_duplicates job_name obj =
  let keys = Hashtbl.create 10 in
  List.iter
    (fun (k, _) ->
      let count = succ @@ Option.value ~default:0 (Hashtbl.find_opt keys k) in
      Hashtbl.replace keys k count)
    (YAML.of_object_exn obj) ;
  let dups =
    Hashtbl.fold (fun k v dups -> if v > 1 then k :: dups else dups) keys []
  in
  if List.length dups > 0 then
    Format.eprintf
      "Warning: Template key overriding support is naive. Duplicate keys in \
       job `%s`: %s\n"
      job_name
      (String.concat ", " dups)

(** Replaces the [extends: v] key-value pair, if present, in the obj
   [job] using the values in [template_definitions]. solve_extensions
   is called recursively until all extends have been resolved. *)
let rec solve_extensions ~debug_template_extension (recursion_depth : int)
    (template_definitions : (string * Yaml.value) list)
    ((job_name, job_desc) as job) : string * Yaml.value =
  let job_desc =
    YAML.of_object_exn
      ~err:"Only job as objects are supported for the moment"
      job_desc
  in
  match get_extend_names job_desc with
  | [] ->
      job
  | extend_names ->
      let extend_content : Yaml.value list =
        List.map
          (fun template_name ->
            match List.assoc_opt template_name template_definitions with
            | Some template_definition ->
                let (_, expanded_template_definition) =
                  solve_extensions
                    ~debug_template_extension
                    (succ recursion_depth)
                    template_definitions
                    (template_name, template_definition)
                in
                if debug_template_extension then
                  let marker_prefix typ tag =
                    String.make (recursion_depth + 2) '_'
                    ^ "glcisk_extends_" ^ typ ^ "_" ^ tag ^ "_"
                  in
                  expanded_template_definition
                  |> YAML.object_prepend_field
                       (marker_prefix "begin" template_name)
                       (`String "begin")
                  |> YAML.object_append_field
                       (marker_prefix "__end" template_name)
                       (`String "end")
                else expanded_template_definition
            | None ->
                failwith
                @@ Format.asprintf
                     "Could not find template `%s` when resolving `extends` \
                      in job `%s`, available definitions: %s"
                     template_name
                     job_name
                     (String.concat ", " (List.map fst template_definitions)))
          extend_names
      in
      let extend_contents_merged = YAML.merge_objects_append ~recursive:true extend_content in
      (* check_duplicates job_name extend_contents_merged ; *)
      let expanded_job_desc =
        replace_extend_by_definition
          ~debug_template_extension
          job_name
          (`O job_desc)
          extend_contents_merged
      in
      (job_name, expanded_job_desc)

let solve_extensions ~debug_template_extension template_definitions jobs =
  List.map
    (fun job ->
      if field_is_special_section (fst job) then job
      else
        solve_extensions ~debug_template_extension 0 template_definitions job)
    jobs

let remove_include_section yaml_objects =
  let section_to_remove = ["include"] in
  List.filter (fun (k, _) -> not (List.mem k section_to_remove)) yaml_objects

type template_extensions_strategy =
  | Expand
  | No_expansion
  | Debug_expansion

let merge ?(template_extensions_strategy=No_expansion) base_dir yaml_object =
  let all_includes = get_other_yaml_paths base_dir yaml_object in
  let yaml_objects = List.concat [yaml_object; all_includes] in
  (* print_endline (Yaml.to_string_exn (`O yaml_objects)) ; *)
  let (_, templates, _) = partition yaml_objects in
  (* let (extends, _) = filter_extend_definitions yaml_objects in *)
  let yaml_objects = remove_include_section yaml_objects in
  match template_extensions_strategy with
  | Expand -> solve_extensions ~debug_template_extension:false templates yaml_objects
  | Debug_expansion -> solve_extensions ~debug_template_extension:true templates yaml_objects
  | No_expansion -> yaml_objects
  (* in *)
  (* expanded_yaml_objects *)

let stages (merged_ci_conf : (string * Yaml.value) list) =
  List.assoc_opt "stages" merged_ci_conf
  |> Option.map YAML.of_array_exn
  |> Option.map (List.map YAML.of_string_exn)
  (* If no stages are defined in .gitlab-ci.yml, then
     build, test and deploy are the default pipeline stages.  *)
  |> Option.value ~default:["build"; "test"; "deploy"]

let stage (job : string * Yaml.value) =
  List.assoc_opt "stage" (YAML.of_object_exn (snd job))
  |> Option.map YAML.of_string_exn
  (* the default stage is "test" *)
  |> Option.value ~default:"test"
