type ('a, 'b) tree = Leaf of 'a | Node of ('b * ('a, 'b) tree list)

(*

 - santity
   - foo
   - bar
 - build
   - build;

*)

let rec iter (f_leaf : 'a -> unit) (f_node : 'b -> unit) = function
  | Leaf lc ->
      f_leaf lc
  | Node (nc, children) ->
      f_node nc ;
      List.iter (iter f_leaf f_node) children

let iter_d (f_leaf : int -> 'a -> unit) (f_node : int -> 'b -> unit) t =
  let rec iter_daux depth = function
    | Leaf lc ->
        f_leaf depth lc
    | Node (nc, children) ->
        f_node depth nc ;
        List.iter (iter_daux (succ depth)) children
  in
  iter_daux 0 t

let pp ?(indent_width = 2) pp_leaf pp_node fmt =
  let indent_string ind = String.make (indent_width * ind) ' ' in
  iter_d
    (fun depth leaf_contents ->
      Format.fprintf fmt "%s- %a\n" (indent_string depth) pp_leaf leaf_contents)
    (fun depth node_contents ->
      Format.fprintf fmt "%s- %a\n" (indent_string depth) pp_node node_contents)

(* module Test = struct
 * 
 *   let ex1 = Node ("Foo", [
 *               Leaf 1 ;
 *               Leaf 2 ;
 *               Node ("Bar", [
 *                   Leaf 3 ;
 *                   Leaf 4 ;
 *                 ])
 *             ])
 * 
 *   let pp fmt t = pp (Format.pp_print_int) (Format.pp_print_string) fmt t
 * 
 *   let () =
 *     let print t = Format.printf "%a" pp t in
 *     print ex1
 * end *)

(* let rec pp_aux (indentation : int) = function
 *   | Leaf leaf_contents -> Format.fprintf
 *                             fmt
 *                             "%s- %a\n"
 *                             (indent_string indentation)
 *                             pp_leaf leaf_contents
 *   | Node node_contents ->
 *      Format.fprintf
 *        fmt
 *        "%s- %a\n"
 *        (indent_string indentation)
 *        pp_node node_contents
 * in
 * pp_aux 0 t *)
