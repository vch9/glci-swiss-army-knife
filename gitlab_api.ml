open Cohttp_lwt_unix
open Tezos_error_monad
open Error_monad

type config = {access_token : string option}

let default_config = {access_token = None}

(* Make a get call and return the body parsed as json.
 * You could use ppx_deriving_yojson or atdgen to
 * reduce the json parsing boilerplate, but for such small
 * example is overkill.
 *
 * val json_body : string -> Basic.json Lwt.t
 *)
let json_body uri =
  Client.get (Uri.of_string uri)
  >>= fun (_resp, body) ->
  (* here you could check the headers or the response status
   * and deal with errors, see the example on cohttp repo README *)
  body |> Cohttp_lwt.Body.to_string >|= Yojson.Basic.from_string

let lint ?(include_merged_yaml=false) ~(config : config) (ci : string) =
  let endpoint =
    "https://gitlab.com/api/v4/ci/lint?include_merged_yaml=false"
  in
  let body_json =
    `Assoc [
        ("content", `String ci);
        ("include_merged_yaml", `Bool include_merged_yaml)
      ] in
  let json_s = Yojson.Basic.to_string body_json in
  (* Format.printf "Sending json string: %s\n" (String.sub json_s 0 100 ^ " ...") ; *)
  let body = Cohttp_lwt.Body.of_string @@ json_s in
  let headers =
    Cohttp.Header.(add (init ()) "Content-Type" "application/json")
  in
  let headers =
    Option.fold
      ~none:headers
      ~some:(Cohttp.Header.add headers "PRIVATE-TOKEN")
      config.access_token
  in
  Cohttp_lwt_unix.Client.post ~headers ~body (Uri.of_string endpoint)
  >>= fun (_resp, body_resp) ->
  (* Format.printf "Response object: %a\n"
   *   Cohttp.Response.pp_hum _resp; *)
  Cohttp_lwt.Body.to_string body_resp
  >>= fun body_resp_s ->
  (* Format.printf "Response content: %s\n"
   *   ( body_resp_s) ; *)
  return (Yojson.Basic.from_string body_resp_s)
