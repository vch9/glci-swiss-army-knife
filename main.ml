open Tezos_clic
open Tezos_error_monad
open Error_monad

type gl_context = {
  base_dir : Fpath.t;
  ci_config : (string * Yaml.value) list;
  scalar_style : Yaml.scalar_style;
  gitlab_api_config : Gitlab_api.config;
  sort_keys : bool;
}

let default_gl_context : gl_context =
  {
    ci_config = [];
    base_dir = Fpath.v ".";
    scalar_style = `Single_quoted;
    gitlab_api_config = Gitlab_api.default_config;
    sort_keys = false;
  }

let group = {Clic.name = "gitlab"; title = "Gitlab CI file commands"}

let yaml_scalar_styles =
  [ ("plain", `Plain);
    ("single-quoted", `Single_quoted);
    ("any", `Any);
    ("double-quoted", `Double_quoted);
    ("folded", `Folded);
    ("literal", `Literal) ]

let extension_strategies =
  let open Gitlab in
  [ ("no-expansion", No_expansion);
    ("debug-expansion", Debug_expansion);
    ("expand", Expand); ]

module GlClic = struct
  (* Parameters *)
  let path_parameter =
    Clic.parameter (fun (_ctxt : gl_context) p ->
        (* if Sys.file_exists p then return p *)
        (* else failwith "Not an existing file: '%s'" p *)
        return p)

  let int_parameter =
    Clic.parameter (fun (_ctxt : gl_context) p -> return @@ int_of_string p)

  let gitlab_conf_parameter =
    Clic.parameter (fun (_ctxt : gl_context) access_token ->
        return Gitlab_api.{access_token = Some access_token})

  let scalar_style_parameter =
    Clic.parameter (fun (_ctxt : gl_context) param ->
        match List.assoc_opt param yaml_scalar_styles with
        | Some style ->
            return style
        | None ->
            failwith
              "Unknown quoting style: '%s'. Valid choices: %s"
              param
              (String.concat ", " @@ List.map fst yaml_scalar_styles))

  let extension_strategy_parameter =
    Clic.parameter (fun (_ctxt : gl_context) param ->
        match List.assoc_opt param extension_strategies with
        | Some strategy ->
            return strategy
        | None ->
            failwith
              "Unknown extension strategy: '%s'. Valid choices: %s"
              param
              (String.concat ", " @@ List.map fst extension_strategies))

  (* Args *)
  let template_extensions_strategy_arg ~default =
    Clic.default_arg
      ~doc:(Format.asprintf
              "Template extension strategy used. Valid choices: %s"
              (String.concat ", " @@ List.map fst extension_strategies))
      ~short:'t'
      ~long:"template-extension"
      ~placeholder:"strategy"
      ~default
      extension_strategy_parameter
end

module GlYaml = struct
  let to_string_exn ~scalar_style obj = Yaml.to_string_exn ~scalar_style obj

  let to_string_err ~scalar_style v =
    match Yaml.to_string ~scalar_style v with
    | Ok s ->
        return s
    | Error (`Msg msg) ->
        failwith "%s" msg

  let roundabout_to_string_exn ~scalar_style obj =
    match obj with
    | `O kvs ->
        List.map (fun (k, v) -> to_string_exn ~scalar_style (`O [(k, v)])) kvs
        |> String.concat ""
    | v ->
        to_string_exn ~scalar_style v

  let roundabout_to_string_err ~scalar_style obj =
    match obj with
    | `O kvs ->
        (* this works *)
        Util.map_es
          (fun (k, v) -> to_string_err ~scalar_style (`O [(k, v)]))
          kvs
        >|=? String.concat ""
    (* this doesnt: ?? *)
    (* Yaml.pp (`O obj) ; *)
    | v ->
        to_string_err ~scalar_style v

  let pp_roundabout ~scalar_style fmt obj : unit =
    Format.fprintf fmt "%s" (roundabout_to_string_exn ~scalar_style obj)

  let to_string_roundabout ~scalar_style obj =
    Format.asprintf "%a" (pp_roundabout ~scalar_style) obj
end

module Global_options = struct
  let access_token =
    Clic.default_arg
      ~doc:"Gitlab Access token"
      ~short:'t'
      ~long:"gl-access-token"
      ~placeholder:"<TOKEN>"
      ~default:"<TOKEN>"
      GlClic.gitlab_conf_parameter

  let ci_file =
    Clic.default_arg
      ~doc:"The .gitlab-ci.yml file"
      ~short:'f'
      ~long:"ci-file"
      ~placeholder:".gitlab-ci.yml"
      ~default:".gitlab-ci.yml"
      GlClic.path_parameter

  let scalar_style =
    Clic.default_arg
      ~doc:(Format.asprintf
              "Scalar style used. Valid choices: %s"
              (String.concat ", " @@ List.map fst yaml_scalar_styles))
      ~short:'s'
      ~long:"scalar-style"
      ~placeholder:"style"
      ~default:(fst @@ List.hd yaml_scalar_styles)
      GlClic.scalar_style_parameter

  let sort_keys_arg =
    Clic.switch
      ~doc:"Sort keys of YAML hashes in output"
      ~short:'k'
      ~long:"sort-keys"
      ()

  let options =
    Clic.args4 access_token ci_file scalar_style sort_keys_arg
end

let output ~sort_keys ~scalar_style obj =
  let obj =
    if sort_keys
    then YAML.sort_keys_recursively obj
    else obj
  in
  print_string (GlYaml.to_string_roundabout ~scalar_style obj)

(* let file_printer ?(file = ".gitlab-ci.yml") obj =
 *   Bos.OS.File.write Fpath.(v file) (GlYaml.pp obj) *)

module Action_focus = struct
  let options =
    let arg_no_preamble =
      Clic.switch
        ~doc:"Do not print the CI preamble"
        ~short:'p'
        ~long:"no-preamble"
        ()
    in
    Clic.args2 arg_no_preamble
      (GlClic.template_extensions_strategy_arg ~default:"expand")

  let params =
    Clic.(
      prefix "focus" @@ string ~name:"job" ~desc:"Job we want to focus" @@ stop)

  let focus :
      (bool * Gitlab.template_extensions_strategy) ->
      string ->
      gl_context ->
      unit Tezos_error_monad.Error_monad.tzresult Lwt.t =
   fun (no_preamble, template_extensions_strategy)
       focused_job
       {ci_config; base_dir; scalar_style; sort_keys; _} ->
    let expanded_yaml_objects =
      Gitlab.merge ~template_extensions_strategy base_dir ci_config
    in
    let (special_sections, _, jobs) = Gitlab.partition expanded_yaml_objects in
    match List.assoc_opt focused_job expanded_yaml_objects with
    | None ->
        failwith
          "No job `%s`. Available jobs:\n@[<v>%a@]"
          focused_job
          (Format.pp_print_list
             ~pp_sep:Format.pp_print_newline
             Format.pp_print_string)
          (List.map fst jobs)
    | Some filtered_job ->
        let preamble = if no_preamble then [] else special_sections in
        let final_content = preamble @ [(focused_job, filtered_job)] in
        output ~sort_keys ~scalar_style (`O final_content) ;
        return_unit

  let command = Clic.command ~desc:"Focus a job" ~group options params focus
end

module Action_merge = struct
  let options =
    Clic.args1
      (GlClic.template_extensions_strategy_arg ~default:"no-expansion")

  let params = Clic.(prefix "merge" @@ stop)

  let merge :
      (Gitlab.template_extensions_strategy) ->
      gl_context ->
      unit Tezos_error_monad.Error_monad.tzresult Lwt.t =
   fun template_extensions_strategy {ci_config; base_dir; scalar_style; sort_keys; _} ->
    let expanded_yaml_objects =
      Gitlab.merge ~template_extensions_strategy base_dir ci_config
    in
    output ~sort_keys ~scalar_style (`O expanded_yaml_objects) ;
    return_unit

  let command = Clic.command ~desc:"Merge CI file" ~group options params merge
end

module Action_lint = struct
  let options =
    let include_merged_yaml =
      Clic.switch
        ~doc:"Output the merged yaml"
        ~short:'m'
        ~long:"include-merged-yaml"
        ()
    in
    Clic.args1 include_merged_yaml

  let params = Clic.(prefix "lint" @@ stop)

  let format_results (res : Yojson.Basic.t) =
    let open JSON in
    match Option.bind (get_field_opt res "status") of_string_opt with
    | Some status ->
        Option.iter
           (fun s -> Format.printf "Merged YAML:\n%s\n" s)
           (Option.bind (get_field_opt res "merged_yaml") of_string_opt) ;
        Format.printf "Status: %s\n" status ;
        (match status with
        | "valid" -> ()
        | "invalid" ->
          (match Option.bind (get_field_opt res "errors") of_list_opt with
           | Some errors ->
              Format.printf "Errors:\n";
              List.iter (fun e -> Format.printf " - %s\n" (of_string_exn e)) errors
           | None ->
              Format.printf "Unknown error:\n%a" Yojson.Basic.pp res)
        | _ ->
          Format.printf "Errors: \n%a\n" Yojson.Basic.pp res)
    | None -> (
      match Option.bind (get_field_opt res "message") of_string_opt with
      | Some ("401 Unauthorized" as msg) ->
          Stdlib.failwith
            (Format.asprintf
               "Received message `%s`, consider passing `--gl-access-token`"
               msg)
      | Some msg ->
          Stdlib.failwith (Format.asprintf "Received message `%s`" msg)
      | None ->
          Stdlib.failwith
            (Format.asprintf
               "not field `status` nor `message` field in `%a`"
               (Yojson.Basic.pretty_print ~std:true)
               res) )

  let merge :
      bool -> gl_context -> unit Tezos_error_monad.Error_monad.tzresult Lwt.t =
   fun include_merged_yaml
       { ci_config;
         base_dir;
         scalar_style;
         gitlab_api_config;
         _ } ->
    let ci_config =
      Gitlab.merge base_dir ci_config
    in
    GlYaml.roundabout_to_string_err ~scalar_style (`O ci_config)
    >>=? Gitlab_api.lint ~include_merged_yaml ~config:gitlab_api_config
    >>=? fun res -> format_results res ; return_unit

  let command = Clic.command ~desc:"Lint CI file" ~group options params merge
end

module Action_visualize = struct
  let options =
    let cutoff_arg =
      Clic.arg
        ~doc:
          "Show only first (if positive) or last (if negative) <n> jobs per \
           stage"
        ~short:'c'
        ~long:"cutoff"
        ~placeholder:"n"
        (* ~default:10 *)
        GlClic.int_parameter
    in
    Clic.args1 cutoff_arg

  let params = Clic.(prefixes ["visualize"; "pipeline"] @@ stop)

  let rec sublist_opt b e l =
    match l with
    | [] ->
        None
    | h :: t ->
        let tail = if e = 0 then Some [] else sublist_opt (b - 1) (e - 1) t in
        if b > 0 then tail else Option.map (List.cons h) tail

  let cutoff placeholder n l =
    let len = List.length l in
    if len <= abs n then l
    else if n = 0 then [placeholder]
    else if n >= 0 then Option.get (sublist_opt 0 (n - 1) l) @ [placeholder]
    else placeholder :: Option.get (sublist_opt (len + n) (len - 1) l)

  let visualize :
      int option ->
      gl_context ->
      unit Tezos_error_monad.Error_monad.tzresult Lwt.t =
   fun cutoff_opt {ci_config; base_dir; _} ->
    let ci_config =
      Gitlab.merge base_dir ci_config
    in
    let (_, _, jobs) = Gitlab.partition ci_config in
    let bin : ('v -> 'k) -> 'v list -> ('k, 'v list) Hashtbl.t =
     fun f_label l ->
      List.fold_right
        (fun el bins ->
          let lbl = f_label el in
          let bin = Hashtbl.find_opt bins lbl |> Option.value ~default:[] in
          Hashtbl.replace bins lbl (el :: bin) ;
          bins)
        l
        (Hashtbl.create 5)
    in
    let jobs_by_stage = bin Gitlab.stage jobs in
    let tree =
      let open Tree in
      Node
        ( "CI Pipeline",
          List.map
            (fun stage ->
              let jobs =
                Hashtbl.find_opt jobs_by_stage stage
                |> Option.value ~default:[]
              in
              let leafs = List.map (fun job -> Leaf (fst job)) jobs in
              let leafs =
                match cutoff_opt with
                | None ->
                    leafs
                | Some n ->
                    cutoff (Leaf "<...>") n leafs
              in
              Node (stage, leafs))
            (Gitlab.stages ci_config) )
    in
    let pp_stage fmt s = Format.fprintf fmt "[%s]" s in
    Format.printf "%a" (Tree.pp Format.pp_print_string pp_stage) tree ;
    return_unit

  let command =
    Clic.command
      ~desc:"Visualize pipeline defined by CI file"
      ~group
      options
      params
      visualize
end

let commands =
  [ Action_focus.command;
    Action_merge.command;
    Action_lint.command;
    Action_visualize.command ]

let commands_with_man =
  Clic.add_manual
    ~executable_name:(Filename.basename Sys.executable_name)
    ~global_options:Global_options.options
    (if Unix.isatty Unix.stdout then Clic.Ansi else Clic.Plain)
    Format.std_formatter
    commands

let setup_context
    (gitlab_api_config, ci_config_path, scalar_style, sort_keys)
    =
  ( if Sys.file_exists ci_config_path then return ()
  else failwith "Not an existing file: '%s'" ci_config_path )
  >>=? fun () ->
  let ci_config_path = Fpath.v ci_config_path in
  let base_dir = fst (Fpath.split_base ci_config_path) in
  let ci_config = Yaml_unix.of_file_exn ci_config_path in
  match ci_config with
  | `O ci_config ->
      return
        {
          ci_config;
          base_dir;
          scalar_style;
          gitlab_api_config;
          sort_keys;
        }
  | _ ->
      failwith "Invalid GitLab CI file %a" Fpath.pp ci_config_path

let () =
  ignore
    Clic.(
      setup_formatter
        Format.std_formatter
        (if Unix.isatty Unix.stdout then Ansi else Plain)
        Short) ;
  let original_args = Array.to_list Sys.argv |> List.tl in
  let result =
    Lwt_main.run
      ( Clic.parse_global_options
          Global_options.options
          default_gl_context
          original_args
      >>=? fun (global_args, args) ->
      setup_context global_args
      >>=? fun ctx -> Clic.dispatch commands_with_man ctx args )
  in
  match result with
  | Ok global_options ->
      global_options
  | Error [Clic.Help command] ->
      Clic.usage
        Format.std_formatter
        ~executable_name:(Filename.basename Sys.executable_name)
        ~global_options:Global_options.options
        (match command with None -> [] | Some c -> [c]) ;
      exit 0
  | Error errors ->
      Clic.pp_cli_errors
        Format.err_formatter
        ~executable_name:(Filename.basename Sys.executable_name)
        ~global_options:Clic.no_options
        ~default:(fun fmt err -> Error_monad.pp_print_error fmt [err])
        errors ;
      exit 1
