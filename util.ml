open Tezos_error_monad
open Error_monad

(* The functions rev_map_es and map_es implement a shim for the
   upcoming Tezos_lwt_result_stdlib.Lwtreslib.List.map_es. It can be
   removed once that lib is released.
*)

let rev_map_es f l =
  let rec aux ys = function
    | [] ->
        return ys
    | x :: xs ->
        f x >>=? fun y -> (aux [@ocaml.tailcall]) (y :: ys) xs
  in
  match l with
  | [] ->
      return []
  | x :: xs ->
      Lwt.apply f x >>=? fun y -> aux [y] xs

(* shim for Tezos_lwt_result_stdlib.Lwtreslib.List.map_es *)
let map_es f l = rev_map_es f l >|=? List.rev

(* todo: unused *)
let assoc_def ~default key obj =
  List.assoc_opt key obj |> Option.value ~default
