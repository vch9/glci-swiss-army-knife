#!/bin/bash

ci_file="$1"
job1="$2"
job2="$3"

tmp1=$(mktemp)
dune exec ./main.exe -- --ci-file "$ci_file" focus "$job1" --no-preamble > $tmp1

tmp2=$(mktemp)
dune exec ./main.exe -- --ci-file "$ci_file" focus "$job2" --no-preamble > $tmp2

diff --color -u "$tmp1" --label "$job1" "$tmp2" --label "$job2"
