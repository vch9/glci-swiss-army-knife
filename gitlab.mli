type template_extensions_strategy =
  | Expand
  | No_expansion
  | Debug_expansion

val merge :
  ?template_extensions_strategy:template_extensions_strategy ->
    (* :template_extensions_strategy -> *)
  Fpath.t ->
  (string * Yaml.value) list ->
  (string * Yaml.value) list

val partition :
  (string * Yaml.value) list ->
  (string * Yaml.value) list * (string * Yaml.value) list * (string * Yaml.value) list

val stage : string * Yaml.value -> string

val stages : (string * Yaml.value) list -> string list
