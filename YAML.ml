let as_string_opt v = match v with `String s -> Some s | _ -> None

let of_string_exn ?err v =
  match v with
  | `String s ->
      s
  | _ ->
      failwith
      @@ Option.value err ~default:"Expected string"
      ^ ", when reading: "
      ^ Yaml.to_string_exn ~scalar_style:`Plain v

(* replace extends section of the job. Not using a hashmap because we need
   to keep the order. We should stop directly when we found the key "extends" *)
let of_object_exn ?err v =
  match v with
  | `O l ->
      l
  | _ ->
      failwith
      @@ Option.value err ~default:"Expected object"
      ^ ", when reading: "
      ^ Yaml.to_string_exn ~scalar_style:`Plain v

(* replace extends section of the job. Not using a hashmap because we need
   to keep the order. We should stop directly when we found the key "extends" *)
let of_array_exn ?err v =
  match v with
  | `A l ->
      l
  | _ ->
      failwith
      @@ Option.value err ~default:"Expected array"
      ^ ", when reading: "
      ^ Yaml.to_string_exn ~scalar_style:`Plain v

let get name (node : Yaml.value) =
  match node with
  | `O fields -> (
    match List.assoc_opt name fields with None -> `Null | Some node -> node )
  | _ ->
      `Null

let ( |-> ) node field = get field node

let concat_objects (os : Yaml.value list) : Yaml.value =
  `O (List.concat @@ List.map of_object_exn os)

let merge_objects (os : Yaml.value list) : Yaml.value =
  let fields = List.concat @@ List.map of_object_exn os in
  let tbl = Hashtbl.create 30 in
  List.iter (fun (k, v) -> Hashtbl.replace tbl k v) fields ;
  `O (List.of_seq @@ Hashtbl.to_seq tbl)

let rec replace_assoc_inplace k v = function
  | [] ->
      [(k, v)]
  | (k', v') :: tl ->
      if k = k' then (k, v) :: tl else (k', v') :: replace_assoc_inplace k v tl

let object_set (k : string) (v : Yaml.value) (o : Yaml.value) : Yaml.value =
  let fields = of_object_exn o in
  `O (replace_assoc_inplace k v fields)

let object_rem (k : string) (o : Yaml.value) : Yaml.value =
  let fields = of_object_exn o in
  `O (List.remove_assoc k fields)

let object_prepend_field (k : string) (v : Yaml.value) (o : Yaml.value) :
    Yaml.value =
  let fields = of_object_exn o in
  `O ((k, v) :: fields)

let object_append_field (k : string) (v : Yaml.value) (o : Yaml.value) :
    Yaml.value =
  let fields = of_object_exn o in
  `O (fields @ [(k, v)])

let merge_objects_inplace (os : Yaml.value list) : Yaml.value =
  let fields = List.concat @@ List.map of_object_exn os in
  let fields_merged =
    List.fold_left
      (fun fields' (k, v) -> replace_assoc_inplace k v fields')
      []
      fields
  in
  `O fields_merged

let rec merge_objects_append ?(recursive=false) (os : Yaml.value list) : Yaml.value =
  let fields = List.concat @@ List.map of_object_exn os in
  let fields_merged =
    List.fold_left
      (fun fields' (k, v) -> replace_assoc_append ~recursive k v fields')
      []
      fields
  in
  `O fields_merged
and replace_assoc_append ?(recursive=false) k' v' = function
  | [] ->
      [(k', v')]
  | (k, v) :: tl ->
      if k = k'
      then
        let v'' =
          match (v, v') with
          | (`O _o, `O _o') when recursive ->
             merge_objects_append ~recursive [v; v']
          | _ ->
             v'
        in
        tl @ [(k', v'')]
      else
        (k, v) :: (replace_assoc_append ~recursive k' v' tl)

let rec sort_keys_recursively (v : Yaml.value) : Yaml.value =
  match v with
  | `O bindings ->
     let bindings = List.map (fun (k,v) -> (k, sort_keys_recursively v)) bindings in
     `O (List.sort (fun (k, _) (k', _) -> String.compare k k') bindings)
  | `A l -> `A (List.map sort_keys_recursively l)
  | _ -> v
