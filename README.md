# The GitLab CI Swiss Army Knife

This tool has been developed for the
[Tezos](https://gitlab.com/tezos/tezos) Gitlab CI configuration, but
it's usage is general. It can be used to lint configurations (using
GitLab's api), to merge multi-file configurations into single-file
configurations, debugging template extension and comparing
configurations.

## Installation

```shell
opam install .
```

## Usage

The Swiss knife is invoked thus:

``` shell
dune exec \
  ./main.exe -- \
  --ci-file "$HOME/devel/tezos/.gitlab-ci.yml" \
  ACTION
```

The argument `--ci-file` defaults to `.gitlab-ci.yml` so can be
omitted to act on GitLab CI configuration in the working directory.

For ease of use, I suggest adding the following function to your shell:

``` shell
glcisk() {
    GLCISK_PATH=~/dev/nomadic-labs/glci-swiss-knife
    ( cd $GLCISK_PATH
      eval $(opam env)
      dune build ./main.exe
      cd - ) >&2
    $GLCISK_PATH/_build/default/main.exe $@
}
```

after which the knife can be invoked through `glcisk` (used below).

### Merging

The main functionality is merging the CI files. This will inline all
inclusions (from the `include:` section) and output the resulting
file. Optionally, you can also inline template expansion using the
`--template-extension` flag.

```shell
glcisk merge
```

or

```shell
glcisk merge --template-extension expand
```

### Linting

You can also run lint the configuration using GitLab's Linting API:
```shell
glcisk lint
```

If you are interested in the merged YAML as reported back by GitLab's Linting API, pass the `--include-merged-yaml` to this command:

```shell
glcisk lint --include-merged-yaml
```

Linting may require a GitLab Acccess token. This can be passed using
the `--gl-access-token` global option.


```shell
glcisk --gl-access-token ACCESS_TOKEN lint
```

### Focus

If you want to generate the CI file for only one job (here, the `build` job), you can use the `focus` action:
```shell
glcisk focus build
```

The default template expansion strategy here inlines all extensions,
as the output will not include any templates on which the job
depends. This can be overridden using the `--template-extension` flag.

### Debugging template extensions

Job definitions using multi-level template expansion can be a bit
tricky to debug. The Swiss knife comes with a template extensions
debug option (invoked by passing `--template-extensions
debug-expansion` to the actions `focus` or `merge`) that will add
dummy keys to the expanded job definitions indicating the origin of a
jobs values.

Consider the following output obtained by invoking
`glcisk focus release-static-arm64-binaries --template-extensions debug-expansion`:

```yaml
release-static-arm64-binaries:
  __glcisk_extends_begin_.release_static_binaries_template_: begin
  ___glcisk_extends_begin_.rules_template__release_tag_: begin
  rules:
  - if: $CI_COMMIT_TAG =~ /\A\d+\.\d+\.\d+\z/ && $CI_PROJECT_NAMESPACE == "tezos"
    when: on_success
  - when: never
  ___glcisk_extends___end_.rules_template__release_tag_: end
  image: registry.gitlab.com/gitlab-org/release-cli
  stage: publish_release
  script:
  - apk --no-cache --virtual add bash jq curl
  - scripts/release/upload-static-binaries-to-package-registry.sh "$ARCH_PREFIX"
  __glcisk_extends___end_.release_static_binaries_template_: end
  __glcisk_job_definition: release-static-arm64-binaries
  variables:
    PACKAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/tezos/${CI_COMMIT_TAG}
    ARCH_PREFIX: arm64-
  dependencies:
  - build_release:static-arm64-linux-binaries
```

The dummy keys are prefixed `__glcisk_` (the number of initial
underscores indicate template extension depth). The above result is
read thus: `release-static-arm64-binaries` has extended the
`.release_static_binaries_template` template. The result of this
template is between the dummy keys:

 - `__glcisk_extends_begin_.release_static_binaries_template_: begin`
 - `__glcisk_extends___end_.release_static_binaries_template_: end`

This template in turn extends the `rules_template__release_tag`, from
which the rules section is obtained. Then follows other values from
`.release_static_binaries_template`: `image`, `stage`, `script`. If
some key is defined by this template but overriden by a subsequent
template then it will *not* be present here, but in the expansion of
that subsequent template.

Finally, the `__glcisk_job_definition` dummy key indicates the start of
values that are from the job `release-static-arm64-binaries` itself.

Note that this output is *not* valid a GitLab CI configuration.

### Comparing configurations

GitLab CI Swiss Army Knife can help when refactoring
configurations. Expanding and diffing a configuration before and after
refactoring helps detect unwanted changes:

``` shell
glcisk --sort-keys --ci-file ".gitlab-ci--before.yml" merge > before.yml

glcisk --sort-keys --ci-file ".gitlab-ci--after.yml" merge > after.yml

diff before.yml after.yml
```

In this example, we merge two configurations `.gitlab-ci--before.yml`
and `.gitlab-ci--after.yml` and diff the results. We pass the
`--sort-keys` flag, which sorts the keys of YAML objects in the
output, making the diff easier to read.

## Known problems

There is an issue in how hashes with empty values are handled by the GitLab Linting API. This will result in errors like:

``` shell
$ glcisk lint
Status: invalid
Errors:
 - jobs:unit:011_pthangzh:variables config should be a hash of key value pairs
 - jobs:publish:docker_manual:variables config should be a hash of key value pairs
```

To the best of my knowledge, these errors can be safely ignored.
